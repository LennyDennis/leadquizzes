# AngularAssessment

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.0-rc.4.

## Requirements

1. Angular 2
  
2. Node

## Setup Instructions and Installation

- Clone this repository 
- Open the folder
- On the terminal, `npm install` then `ng serve`.
- If you get such an error `ERROR in Error encountered resolving symbol values statically. Reference to a local (non-exported) symbol 'appRoutes'. Consider exporting the symbol (position 19:7 in the original .ts file` go to the app.module.ts and save(ctrl + s - when using VS Code) and the project should compile successfully

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
